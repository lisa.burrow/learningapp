import ReactDOM from 'react-dom/client';
import React from 'react';


  export default class NameForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {value: ''};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    alert('A name was submitted: ' + this.state.value);
    event.preventDefault();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          First Name:
          <input type="text" value={this.state.value} data-testid="firstname" onChange={this.handleChange} />
        </label>
        <input type="submit" value="Submit" />
      </form>
    );
  }
}

/*const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(<NameForm />);*/

/*Original code that didn't accept input values, just keeping here for future reference*/
/*import './App.css';

function App() {
  const handleSubmit=(event)=> {
    event.preventDefault();
    console.log("function handlesubmit run");
    console.log(event);
  }
  return (
    <div>
      <div className="App">
          <p className="helloWorld">Hello World</p>
      </div>
      <form onSubmit={handleSubmit}>
          <label form="firstname">First name:</label>
          <input type="text" id="firstname" name="firstname" data-testid="firstname"></input>
          <input type="submit" value="Submit"></input>
      </form>
    </div>
  );
}

export default App;
*/