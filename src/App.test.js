import { render, screen } from '@testing-library/react';
import App from './App';

test('does not render learn react link', () => {
  render(<App />);
  const linkElement = screen.queryByText(/learn react/i);
  expect(linkElement).not.toBeInTheDocument();
});


/*test('displays Hello World on the page', () => {
  render(<App />);
  const textElement = screen.getByText("Hello World");
  expect(textElement).toBeInTheDocument();
  expect(textElement).toHaveClass('helloWorld');
})

test('input box is there', () => {
  render(<App />);
  const inputElement = screen.getByTestId("firstname");
  expect(inputElement).toBeInTheDocument();
})*/

